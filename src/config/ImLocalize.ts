import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import * as RNLocalize from 'react-native-localize';
import { LANGUAGES } from '../translations'

const LANG_CODES = Object.keys(LANGUAGES);

const LANGUAGE_DETECTOR = {
  type: 'languageDetector',
  async: true,
  detect: (callback: Function) => {
    const findBestAvailableLanguage =
      RNLocalize.findBestAvailableLanguage(LANG_CODES);

    callback(findBestAvailableLanguage?.languageTag || 'ptBr');
  },
  init: () => { },
  cacheUserLanguage: (language: string) => {
    console.log({language})
  }
};

i18n
  .use(LANGUAGE_DETECTOR)
  .use(initReactI18next)
  .init({
    resources: LANGUAGES,
    react: {
      useSuspense: false
    },
    interpolation: {
      escapeValue: false
    },
  });
