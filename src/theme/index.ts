export {DarkTheme} from './darkTheme'
export {LightTheme} from './lightTheme'


export type Theme = {
    backgroundColor: string
    textColor: string
}