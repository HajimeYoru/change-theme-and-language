import React, { createContext } from 'react'
import { useState } from 'react'
import { useCallback } from 'react'
import { useContext } from 'react'
import { Theme } from '.'
import { DarkTheme } from './darkTheme'
import { LightTheme } from './lightTheme'


type ActionThemeProviderData = {
    changeTheme: () => void
}

type ThemeProviderData = [
    theme: Theme,
    actions: ActionThemeProviderData
]

const ThemeContext = createContext<ThemeProviderData | null>(null)

type ThemeProviderProps = {
    children: React.ReactNode,
    theme?: Theme
}

export const ThemeProvider: React.FC<ThemeProviderProps> = ({
    children,
    theme = LightTheme
}) => {
    const [currentTheme, setCurrentTheme] = useState(theme)

    const changeTheme = useCallback(() => {
        setCurrentTheme(prevTheme => {
            const selectedTheme =
                prevTheme.backgroundColor === LightTheme.backgroundColor ?
                    DarkTheme : LightTheme
            return selectedTheme
        })
    }, [])

    return (
        <ThemeContext.Provider
            children={children}
            value={[
                theme = currentTheme,
                { changeTheme }
            ]}
        />
    )
}

export const useTheme = () => {
    const context = useContext(ThemeContext)

    if (!context)
        throw new Error('This hook need to be wrapped by ThemeProvider')

    return context
}

type CustomTheme = {
    theme?: Theme
}

// export const withTheme = <Props extends CustomTheme>(WrappedComponent: React.ComponentType<Props>) => {
//     return (
//         <ThemeProvider>
//             <WrappedComponent  />
//         </ThemeProvider>
//     )
// }