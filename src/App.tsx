import React from 'react'
import { SafeAreaView } from 'react-native'
import { Home } from './components/home'
import './config/ImLocalize'
import { ThemeProvider } from './theme/ThemeProvider'


export const App = () => {
  return (
    <ThemeProvider >
      <SafeAreaView style={{
        flex: 1,
      }}>
        <Home />
      </SafeAreaView>
    </ThemeProvider>
  )
}