import {common as en} from './en'
import {common as ptBr} from './pt-br'

const LANGUAGES = {
    ptBr,
    en,
}

export {
    LANGUAGES
}