import React from 'react'
import { useTranslation } from 'react-i18next'
import { StyleSheet } from 'react-native'
import { Text, View, TouchableHighlight } from 'react-native'
import { useTheme } from '../theme/ThemeProvider'


export const Home = () => {
	const { i18n, t } = useTranslation()
	const [theme, { changeTheme }] = useTheme()

	const setLanguage = (code: string) =>
		i18n.changeLanguage(code)

	const changeTheme1 = () => {
		changeTheme()
	}

	return (
		<View style={{
			backgroundColor: theme.backgroundColor,
			flex: 1,
			justifyContent: 'center',
			alignItems: 'center'
		}}>
			<Text style={{
				fontSize: 28,
				color: theme.textColor
			}}>{t('common:hello')}</Text>

			<View style={styles.buttonContainer}>
				<TouchableHighlight
					onPress={() => {
						setLanguage('ptBr')
					}}
					style={styles.button}>
					<Text style={{
						fontSize: 15,
						color: theme.textColor
					}}>Portugues</Text>
				</TouchableHighlight>


				<TouchableHighlight
					onPress={() => {
						setLanguage('en')
					}}
					style={styles.button}>
					<Text style={{
						fontSize: 15,
						color: theme.textColor
					}}>Ingles</Text>
				</TouchableHighlight>
			</View>

			<View style={styles.buttonContainer}>
				<TouchableHighlight onPress={changeTheme1} style={styles.button}>
					<Text style={{
						fontSize: 15,
						color: theme.textColor
					}}>Mudar tema</Text>
				</TouchableHighlight>
			</View>

		</View>
	)
}

const styles = StyleSheet.create({
	buttonContainer: {
		flexDirection: 'row',
		justifyContent: 'space-evenly',
		width: '100%',
		marginVertical: 10,
	},
	button: {
		backgroundColor: '#00FFFF',
		padding: 20,
		alignItems: 'center',
		borderRadius: 10,
		minWidth: '30%'
	}
})